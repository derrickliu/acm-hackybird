import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class ImagePanel extends JPanel {

  	private Image background;	
	private ArrayList<MovingImage> pipes;
	private MovingImage bird;
	
	// Constructs a new ImagePanel with the background image specified by the file path given
  	public ImagePanel(String img) {
  		// The easiest way to make images from file paths in Swing
  		this(new ImageIcon(img).getImage());	
  	}

	// Constructs a new ImagePanel with the background image given
  	public ImagePanel(Image img) {
    	background = img;
    	Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));	
    	// Thoroughly make the size of the panel equal to the size of the image
    	// (Various layout managers will try to mess with the size of things to fit everything)
    	setPreferredSize(size);
    	setMinimumSize(size);
    	setMaximumSize(size);
    	setSize(size);
    	
    	pipes = new ArrayList<MovingImage>();
   }

	// This is called whenever the computer decides to repaint the window
	// It's a method in JPanel that is overwritten to paint the background and foreground images
  	public void paintComponent(Graphics g) {
  		// Paint the background with its upper left corner at the upper left corner of the panel
    	g.drawImage(background, 0, 0, null); 
    	// Paint each image in the foreground where it should go
    	for(MovingImage img : pipes)
    		g.drawImage(img.getImage(), (int)(img.getX()), (int)(img.getY()), null);
    	if(bird != null)
    		g.drawImage(bird.getImage(), (int)(bird.getX()), (int)(bird.getY()), null);
    	drawStrings(g);
  	}
 	
  	// Draw text on the display  	
 	public void drawStrings(Graphics g) {
 		g.setFont(new Font("Arial", Font.BOLD, 20));
 		g.drawString("Score: " + HackyBirdCanvas.distance, 30, 30);
 	}
 	
  	// Replaces the list of foreground images with the one given, and repaints the panel
  	public void updateImages(ArrayList<MovingImage> newPipes, MovingImage newBird) {
  		bird = newBird;
  		pipes = newPipes;
  		repaint();	// This repaints stuff... you don't need to know how it works
  	}
}