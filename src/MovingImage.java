import javax.swing.*;
import java.awt.*;

class MovingImage {
	private Image image;
	private double x;			// x position
	private double y;			// y position
	
	// Construct a new Moving Image with image, x position, and y position given
	public MovingImage(Image img, double xPos, double yPos) {
		image = img;
		x = xPos;
		y = yPos;
	}
	
	// Construct a new Moving Image with image (from file path), x position, and y position given
	public MovingImage(String path, double xPos, double yPos) {
		//easiest way to make an image from a file path in Swing
		this(new ImageIcon(path).getImage(), xPos, yPos);	
	}
	
	// Setter/getter methods
	public void setPosition(double xPos, double yPos) {
		x = xPos;
		y = yPos;
	}
	
	public void setImage(String path) {
		image = new ImageIcon(path).getImage();
	}
	
	public void setY(double newY) {
		y = newY;
	}
	
	public void setX(double newX) {
		x = newX;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public Image getImage() {
		return image;
	}
}