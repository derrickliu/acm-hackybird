import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JFrame;

// Credits: Thanks to http://hawkee.com/snippet/6233/ for the original copter code
public class HackyBirdCanvas implements MouseListener {
	private static final int WINDOW_HEIGHT = 504;
	private static final int WINDOW_WIDTH = 900;

	private JFrame background;
	private ImagePanel back;

	public static boolean started;
	public static boolean playedOnce;

	public boolean goingUp;
	static double velocity;

	public static int distance;

	private final static int PIPE_STARTING_X = 600;
	private final static int PIPE_HEIGHT = 400;
	private final static int PIPE_WIDTH = 71;
	private final static int PIPE_SPACING = 300;
	
	private static final int GAP_SIZE = 150;

	public final static int BIRD_XPOS = 200;
	public final static int BIRD_YPOS = 270;
	private static final int MOVE_STEP = 2;
	private static final int GAME_TIMESTEP_MS = 10;
	private static final int FLAP_HEIGHT = 60;
	private static final int NUM_OBSTACLES = 3;
	
	private static final int BIRD_HEIGHT = 40;
	private static final int BIRD_WIDTH = 40;

	public static ArrayList<MovingImage> obstacles;
	public ArrayList<MovingImage> recs;
	public static MovingImage bird;
	static double timeSinceMouseClick;
	
	public static void main(String[] args) {
		new HackyBirdCanvas();
	}
	
	public HackyBirdCanvas() {
		startGame();
	}

	public void startGame() {
		if (!playedOnce) {
			background = new JFrame("HackyBird");
			background.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // close the window when closed
			background.setResizable(false);
			background.setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
			background.setVisible(true);

			back = new ImagePanel("img/bg.png");
			background.add(back);
			back.addMouseListener(this);
		}

		playedOnce = true;
		goingUp = false;
		started = false;
		distance = 0;
		velocity = 0;
		
		obstacles = new ArrayList<MovingImage>();
		bird = new MovingImage("img/clumsy.png", BIRD_XPOS, BIRD_YPOS);
		for (int i = 0; i < NUM_OBSTACLES; i++) {
			// Add top and bottom pipes
			double startingPipeY = randomPipeY();
			obstacles.add(new MovingImage("img/pipe.png", PIPE_STARTING_X + i * PIPE_SPACING, startingPipeY));
			obstacles.add(new MovingImage("img/pipe-flipped.png", PIPE_STARTING_X + i * PIPE_SPACING, startingPipeY + PIPE_HEIGHT + GAP_SIZE));
		}

		startDrawing();
	}

	private double randomPipeY() {
		double pipeYRange = (WINDOW_HEIGHT - GAP_SIZE );
		return Math.random() * pipeYRange - pipeYRange;
	}

	public void startDrawing() {
		long lastUpdatedCopter = System.currentTimeMillis();
		
		while (true) {
			double timeSinceLastUpdate = System.currentTimeMillis() - lastUpdatedCopter;
			
			if (started && timeSinceLastUpdate > GAME_TIMESTEP_MS) {
				lastUpdatedCopter = System.currentTimeMillis();
				distance++;
				updateBird();
				updateObstacles();
			}
			
			back.updateImages(obstacles, bird);
		}
	}
	
	public void updateBird() {		
		timeSinceMouseClick += 1;
		
		if (goingUp) {
			bird.setPosition(BIRD_XPOS, (bird.getY() - (FLAP_HEIGHT)));
			goingUp = false;
			bird.setImage("img/clumsy-flap.png");
		} else {
			velocity = updateVelocity();
			bird.setPosition(BIRD_XPOS, (double) (bird.getY() + velocity));
			bird.setImage("img/clumsy.png");
		}
		
		if (birdCollided()) {
			// Pause it for 500ms to make it clear the crash happened			
			try {
				Thread.sleep(500);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			// Game over, start again			
			startGame();
		}
	}
	
	// Function to make flappy bird's fall exponential in speed
	// A constant velocity would be boring, but also work	
	private double updateVelocity() {
		double velocityMax = 10;
		double fallFactor = 20;
		return Math.min(velocityMax, Math.exp(timeSinceMouseClick / fallFactor));
	}

	private void updateObstacles() {
		for (int i = 0; i < obstacles.size(); i += 2) {
			MovingImage topPipe = obstacles.get(i);
			MovingImage bottomPipe = obstacles.get(i + 1);

			// If the pipe pair has exited the screen			
			if(topPipe.getX() < -1 * PIPE_WIDTH) {
				// Re-add the pipe from the right side				
				topPipe.setX(WINDOW_WIDTH);
				bottomPipe.setX(WINDOW_WIDTH);
				
				// Reset y-coordinates of the pipes				
				double startingPipeY = randomPipeY();
				topPipe.setY(startingPipeY);
				bottomPipe.setY(startingPipeY + PIPE_HEIGHT + GAP_SIZE);
			}

			// In either case, advance the pipe forward			
			topPipe.setX(topPipe.getX() - MOVE_STEP);
			bottomPipe.setX(bottomPipe.getX() - MOVE_STEP);
		}
	}

	// Check if the bird collided with any of the pipes
	public boolean birdCollided() {
		for (int i = 0; i < obstacles.size(); i += 2) {
			if (collidedWithPipe(obstacles.get(i), obstacles.get(i + 1))) {
				return true;
			}
		}
		// If the bird is outside the visible screen, consider it crashed
		return bird.getY() + BIRD_HEIGHT < 0 || bird.getY() > WINDOW_HEIGHT;
	}

	//	Draws rectangles around the pipes and the birds and checks for intersection
	// Returns true if the bird collided with either pipe, false otherwise
	public boolean collidedWithPipe(MovingImage topPipe, MovingImage bottomPipe) {
		Rectangle firstPipeBoundingBox = new Rectangle((int) topPipe.getX(), (int) topPipe.getY(), PIPE_WIDTH, PIPE_HEIGHT);
		Rectangle secondPipeBoundingBox = new Rectangle((int) bottomPipe.getX(), (int) bottomPipe.getY(), PIPE_WIDTH, PIPE_HEIGHT);
		Rectangle copterBoundingBox = new Rectangle((int) bird.getX(),	(int) bird.getY(), BIRD_WIDTH, BIRD_HEIGHT);
		return firstPipeBoundingBox.intersects(copterBoundingBox) || secondPipeBoundingBox.intersects(copterBoundingBox);
	}

	public void mousePressed(MouseEvent e) {
		if (!started) {
			started = true;
		}
		goingUp = true;
		timeSinceMouseClick = 0;
	}
	
	// Mouse events when the mouse enters the screen, is released, clicked or exits.
	// Need to be specified anyway since we are implementing MouseListener
	public void mouseEntered(MouseEvent e) { }
	public void mouseReleased(MouseEvent e) { }
	public void mouseClicked(MouseEvent e) { }
	public void mouseExited(MouseEvent e) { }
}